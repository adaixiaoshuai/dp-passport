package net.chenlin.dp.ids.client.manager;

import net.chenlin.dp.ids.common.entity.SessionData;
import net.chenlin.dp.ids.common.entity.TicketValidateResultDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 校验manager
 * @author zcl<yczclcn@163.com>
 */
public interface AuthCheckManager {

    /**
     * 刷新session：web
     * @param request
     */
    void refreshWebSession(HttpServletRequest request);

    /**
     * 获取SessionData：web
     * @param request
     * @return
     */
     SessionData checkWebSession(HttpServletRequest request);

    /**
     * 校验ticket合法性
     * @param ticket
     * @return
     */
     TicketValidateResultDTO validateTicket(String ticket);

    /**
     * 获取SessionData：app
     * @param request
     * @return
     */
     SessionData checkAppSession(HttpServletRequest request);

    /**
     * 获取SessionData
     * @param sessionId
     * @param loginType
     * @return
     */
     SessionData checkClientSession(String sessionId, Integer loginType);

    /**
     * app登出
     * @param request
     */
    void removeAppSession(HttpServletRequest request);

}
